
    var background      = $('.background');
    var backgroundWidth = background.width();

    function resizeImage()
    {
        var imageWidth   = background.width();
        var imageHeight  = background.height();
        var windowHeight = $(window).height();

        var heightRatio  = windowHeight / imageHeight;
        var widthDiff    = heightRatio * imageWidth;

        background.css({
            width: Math.round(widthDiff)+'px',
            height: windowHeight+'px'
        });
    }
    function deplaceContent()
    {

        if(backgroundWidth < background.width())
        {
            var newVal = ((background.width()-backgroundWidth)+400);
            $('header').css({'margin-left':newVal+'px'});
            $('.container').css({'margin-left':newVal+'px'});
        }
        else
        {
            $('header').css({'margin-left':background.width()+'px'});
            $('.container').css({'margin-left':background.width()+'px'});
        }
    }
    resizeImage();
    deplaceContent();

    $(window).resize(function()
    {
        resizeImage();
        deplaceContent();
    });
    if (background.hasClass('hidden'))
    {
        background.removeClass('hidden').fadeIn('slow', function()
        {
            $(this).show();
        });
    }
    $(function() {
        if (window.PIE) {
            $('.container').each(function() {
                PIE.attach(this);
            });
        }
    });
